import bcrypt from 'bcrypt'
import { userRepository } from '../../../../data/repository'
import { BusinessException, ServerException } from '../../../../exception'
import { validateSaveUser } from "../../../validators/user"

export const save = async (data) => {
    validateSaveUser(data)

    let user = userRepository.findByEmail(data.email)
    if (user._id != null) {
        throw new BusinessException('E-mail já cadastrado!')
    }

    try {
        data.password = bcrypt.hashSync(data.password, 10)
        await userRepository.save(data)
    } catch (error) {
        throw new ServerException("Não foi possível inserir o usuário!")
    }
}