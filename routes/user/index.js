import express from 'express'
import { userService } from '../../domain/service'

const userRouter = express.Router()

userRouter.post('/', async (req, res) => {
    await userService.save(req.body)
    res.json({})
})

export default userRouter