import express from 'express'
import { authService } from '../../domain/service'

const authRouter = express.Router()

authRouter.post('/login', async (req, res) => {
    const loginResult = await authService.login(req.body)
    res.json(loginResult)
})

export default authRouter